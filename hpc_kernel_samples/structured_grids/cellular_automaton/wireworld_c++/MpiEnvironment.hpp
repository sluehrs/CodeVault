#pragma once

#include <limits>

#include <mpi.h>

class MpiEnvironment { // wrapper for creating and destroying the environment
	std::size_t worldRank_{std::numeric_limits<std::size_t>::max()};
	std::size_t worldSize_{0};
	bool isMaster_{false};

  public:
	std::size_t worldRank() const { return worldRank_; }
	std::size_t worldSize() const { return worldSize_; }
	bool isMaster() const { return isMaster_; }

	void swap(MpiEnvironment& first, MpiEnvironment& second) noexcept;

	MpiEnvironment(int& argc, char* argv[]);
	~MpiEnvironment();
	MpiEnvironment(MpiEnvironment&) = delete;
	MpiEnvironment& operator=(MpiEnvironment&) = delete;
	MpiEnvironment(MpiEnvironment&& other) noexcept;
	MpiEnvironment& operator=(MpiEnvironment&& other) noexcept;
};
