module mandelpnetcdf
    use infostruct
    use pnetcdf
    use mpi

    implicit none

    private

    public :: open_pnetcdf, close_pnetcdf, write_to_pnetcdf_file, collect_pnetcdf

contains

! open pnetcdf file
    subroutine open_pnetcdf(fh, var_id, info, blocksize, start, rank)
        integer, intent(out) :: fh
        integer, intent(out) :: var_id
        type(t_infostruct), intent(in) :: info
        integer, dimension(2), intent(in) :: blocksize
        integer, dimension(2), intent(in) :: start
        integer, intent(in) :: rank

        integer :: err
        real, dimension(4) :: attr_real
        integer, dimension(2) :: dim_ids

        err = nf90mpi_create(MPI_COMM_WORLD, "simple.nc", NF90_CLOBBER + NF90_64BIT_OFFSET, MPI_INFO_NULL, fh)

        err = nf90mpi_def_dim(fh, "width", int(info%width,MPI_OFFSET_KIND), dim_ids(1))
        err = nf90mpi_def_dim(fh, "height", int(info%height,MPI_OFFSET_KIND), dim_ids(2))

        err = nf90mpi_put_att(fh, NF_GLOBAL, "type", info%type)
        err = nf90mpi_put_att(fh, NF_GLOBAL, "numprocs", info%numprocs)
        err = nf90mpi_put_att(fh, NF_GLOBAL, "maxiter", info%maxiter)

        attr_real(1) = info%xmin
        attr_real(2) = info%xmax
        attr_real(3) = info%ymin
        attr_real(4) = info%ymax

        err = nf90mpi_put_att(fh, NF_GLOBAL, "position", attr_real)

        err = nf90mpi_def_var(fh,"data",NF90_INT,dim_ids,var_id)

        err = nf90mpi_enddef(fh)

        if (info%type /= 0 .and. info%type /= 1) then
            err = nf90mpi_begin_indep_data(fh)
        end if
    end subroutine open_pnetcdf

    ! write block to pnetcdf file
    subroutine write_to_pnetcdf_file(fh, var_id, info, iterations, width, height, xpos, ypos)
        integer, intent(in) :: fh
        integer, intent(in) :: var_id
        type(t_infostruct), intent(in) :: info
        integer, dimension(:), intent(inout) :: iterations
        integer, intent(in) :: width
        integer, intent(in) :: height
        integer, intent(in) :: xpos
        integer, intent(in) :: ypos

        integer(kind=MPI_OFFSET_KIND), dimension(2) :: start
        integer(kind=MPI_OFFSET_KIND), dimension(2) :: num
        integer err

        start(2) = ypos + 1
        start(1) = xpos + 1
        num(2) = height
        num(1) = width
        if (info%type == 0 .or. info%type == 1) then
            err = nf90mpi_put_var_all(fh,var_id,iterations,start,num)
        else
            err = nf90mpi_put_var(fh,var_id,iterations,start,num)
        end if
    end subroutine write_to_pnetcdf_file

    ! close pnetcdf file
    subroutine close_pnetcdf(fh, filetype, info, rank)
        integer, intent(inout) :: fh
        integer, intent(inout) :: filetype
        type(t_infostruct), intent(in) :: info
        integer, intent(in) :: rank

        integer :: err

        err = nf90mpi_close(fh)
    end subroutine close_pnetcdf

    ! read pnetcdf file
    subroutine collect_pnetcdf(iterations, info)
        integer, dimension(:), pointer :: iterations
        type(t_infostruct), intent(inout) :: info

        integer :: fh
        integer :: var_id
        integer, dimension(2) :: dim_ids
        integer :: err
        integer(kind=MPI_OFFSET_KIND) :: offset
        integer(kind=MPI_OFFSET_KIND), dimension(2) :: start
        integer(kind=MPI_OFFSET_KIND), dimension(2) :: num
        real, dimension(4) :: attr_real

        err = nf90mpi_open(MPI_COMM_SELF, "simple.nc", NF90_NOWRITE, MPI_INFO_NULL, fh)

        err = nf90mpi_inq_varid(fh,"data",var_id)
        err = nf90mpi_inquire_variable(fh,var_id,dimids=dim_ids)
        err = nf90mpi_inquire_dimension(fh,dim_ids(1),len=offset)
        info%width = int(offset,kind(info%width))
        err = nf90mpi_inquire_dimension(fh,dim_ids(2),len=offset)
        info%height = int(offset,kind(info%height))

        err = nf90mpi_get_att(fh,NF_GLOBAL,"type",info%type)
        err = nf90mpi_get_att(fh,NF_GLOBAL,"numprocs",info%numprocs)
        err = nf90mpi_get_att(fh,NF_GLOBAL,"maxiter",info%maxiter)
        err = nf90mpi_get_att(fh,NF_GLOBAL,"position",attr_real)

        info%xmin = attr_real(1)
        info%xmax = attr_real(2)
        info%ymin = attr_real(3)
        info%ymax = attr_real(4)

        start(1) = 1
        start(2) = 1
        num(1) = info%width
        num(2) = info%height

        allocate(iterations(info%width*info%height))
        err = nf90mpi_get_var_all(fh,var_id,iterations,start,num)

        err = nf90mpi_close(fh)
    end subroutine collect_pnetcdf

end module mandelpnetcdf

