module mandelhdf5
    use infostruct
    use mpi
    use hdf5

    implicit none

    private

    public :: open_hdf5, close_hdf5, write_to_hdf5_file, collect_hdf5

contains
    ! open hdf5 file
    subroutine open_hdf5(file_id, dataset, info, blocksize, start, rank)
        integer(kind=HID_T), intent(out) :: file_id
        integer(kind=HID_T), intent(out) :: dataset
        type(t_infostruct), intent(in) :: info
        integer, dimension(2), intent(in) :: blocksize
        integer, dimension(2), intent(in) :: start
        integer, intent(in) :: rank

        integer :: hdferr
        integer(kind=HID_T) :: plist_id
        integer(kind=HID_T) :: dcpl_id
        integer(kind=HID_T) :: filespace
        integer(kind=HID_T) :: attr_dataspace_id
        integer(kind=HID_T) :: attribute_id
        integer(kind=HSIZE_T), dimension(2) :: chunk_dims
        integer(kind=HSIZE_T), dimension(2) :: dimsf
        integer :: data_rank
        integer(kind=HSIZE_T), dimension(1) :: dims
        real, dimension(4) :: attr_data

        call H5open_f(hdferr)

        !  set up file access propterty
        call H5Pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdferr)
        call H5Pset_fapl_mpio_f(plist_id,MPI_COMM_WORLD,MPI_INFO_NULL,hdferr)

        ! create new file collectively and release property list identifier
        call H5Fcreate_f("simple.h5",H5F_ACC_TRUNC_F,file_id,hdferr,H5P_DEFAULT_F,plist_id)
        call H5Pclose_f(plist_id,hdferr)

        if (info%type == 2) then
            ! Create a dataset creation property list and set it to use chunking
            call H5Pcreate_f(H5P_DATASET_CREATE_F, dcpl_id, hdferr)
            chunk_dims(1) = blocksize(1)
            chunk_dims(2) = blocksize(2)
            call H5Pset_chunk_f(dcpl_id, 2, chunk_dims, hdferr)
        else
            dcpl_id = H5P_DEFAULT_F
        end if

        ! create dataspace for dataset
        dimsf(1) = info%width
        dimsf(2) = info%height
        data_rank = size(dimsf)
        call H5Screate_simple_f(data_rank, dimsf, filespace, hdferr)

        ! create  dataset
        call H5Dcreate_f(file_id, "Mandelbrotmenge", H5T_NATIVE_INTEGER, filespace, dataset, &
                       & hdferr, dcpl_id, H5P_DEFAULT_F, H5P_DEFAULT_F)
        call H5Sclose_f(filespace, hdferr)
        if (info%type == 2) then
            call H5Pclose_f(dcpl_id, hdferr)
        end if

        ! Data space for attribute
        dims(1) = 1
        call H5Screate_simple_f(1, dims, attr_dataspace_id, hdferr)
        ! Write attributes
        call H5Acreate_f(dataset, "type", H5T_STD_I32LE, attr_dataspace_id, attribute_id, hdferr, H5P_DEFAULT_F, H5P_DEFAULT_F)
        call H5Awrite_f(attribute_id, H5T_NATIVE_INTEGER, info%type, dims, hdferr)
        call H5Aclose_f(attribute_id, hdferr)

        call H5Acreate_f(dataset, "numprocs", H5T_STD_I32LE, attr_dataspace_id, attribute_id, hdferr, H5P_DEFAULT_F, H5P_DEFAULT_F)
        call H5Awrite_f(attribute_id, H5T_NATIVE_INTEGER, info%numprocs, dims, hdferr)
        call H5Aclose_f(attribute_id, hdferr)

        call H5Acreate_f(dataset, "maxiter", H5T_STD_I32LE, attr_dataspace_id, attribute_id, hdferr, H5P_DEFAULT_F, H5P_DEFAULT_F)
        call H5Awrite_f(attribute_id, H5T_NATIVE_INTEGER, info%maxiter, dims, hdferr)
        call H5Aclose_f(attribute_id, hdferr)
        call H5Sclose_f(attr_dataspace_id, hdferr)

        dims(1) = 4
        call H5Screate_simple_f(1, dims, attr_dataspace_id, hdferr)
        call H5Acreate_f(dataset, "position", H5T_IEEE_F64BE, attr_dataspace_id, attribute_id, hdferr, H5P_DEFAULT_F, H5P_DEFAULT_F)

        attr_data(1) = info%xmin
        attr_data(2) = info%xmax
        attr_data(3) = info%ymin
        attr_data(4) = info%ymax

        call H5Awrite_f(attribute_id, H5T_NATIVE_REAL, attr_data, dims, hdferr)
        call H5Aclose_f(attribute_id, hdferr)
        call H5Sclose_f(attr_dataspace_id, hdferr)
    end subroutine open_hdf5

    ! write block to HDF5 file
    subroutine write_to_hdf5_file(file_id, dataset, info, iterations, width, height, xpos, ypos)
        integer(kind=HID_T), intent(in) :: file_id
        integer(kind=HID_T), intent(in) :: dataset
        type(t_infostruct), intent(in) :: info
        integer, dimension(:), intent(in) :: iterations
        integer, intent(in) :: width
        integer, intent(in) :: height
        integer, intent(in) :: xpos
        integer, intent(in) :: ypos

        integer(kind=HID_T) :: filespace, memspace
        integer(kind=HID_T) :: plist_id
        integer(kind=HSIZE_T), dimension(2) :: offset, num, block, stride
        integer(kind=HSIZE_T), dimension(1) :: dimsm
        integer :: hdferr
        integer :: data_rank

        ! create hyperslab
        call H5Dget_space_f(dataset, filespace, hdferr)

        offset(1) = xpos
        offset(2) = ypos
        num(1) = 1
        num(2) = 1
        block(1) = width
        block(2) = height
        stride(1) = width
        stride(2) = height

        call H5Sselect_hyperslab_f(filespace, H5S_SELECT_SET_F, offset, num, hdferr, stride, block)

        data_rank = 1
        dimsm(1) = width * height
        call H5Screate_simple_f(data_rank, dimsm, memspace, hdferr)

        call H5Pcreate_f(H5P_DATASET_XFER_F, plist_id, hdferr)
        if (info%type == 0 .or. info%type == 1) then
            call H5Pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, hdferr)
        else
            call H5Pset_dxpl_mpio_f(plist_id, H5FD_MPIO_INDEPENDENT_F, hdferr)
        end if

        call H5Dwrite_f(dataset, H5T_NATIVE_INTEGER, iterations, dimsm, hdferr, memspace, filespace, plist_id)

        ! release property list and filespace
        call H5Sclose_f(memspace, hdferr)
        call H5Sclose_f(filespace, hdferr)
        call H5Pclose_f(plist_id, hdferr)
    end subroutine write_to_hdf5_file

    ! close hdf5 file
    subroutine close_hdf5(file_id, dataset, info, rank)
        integer(kind=HID_T), intent(inout) :: file_id
        integer(kind=HID_T), intent(inout) :: dataset
        type(t_infostruct), intent(in) :: info
        integer, intent(in) :: rank

        integer :: hdferr

        call H5Dclose_f(dataset, hdferr)
        call H5Fclose_f(file_id, hdferr)
        call H5close_f(hdferr)
    end subroutine close_hdf5

    ! read HDF5 file
    subroutine collect_hdf5(iterations, info)
        integer, dimension(:), pointer :: iterations
        type(t_infostruct), intent(inout) :: info

        integer(kind=HID_T) :: file_id, dataset, filespace, attr
        integer(kind=HSIZE_T), dimension(2) :: dimsr, maxdimsr
        integer(kind=HSIZE_T), dimension(1) :: dims
        real, dimension(4) :: attr_data
        integer :: hdferr

        call H5open_f(hdferr)

        call H5Fopen_f("simple.h5", H5F_ACC_RDONLY_F, file_id, hdferr, H5P_DEFAULT_F)
        call H5Dopen_f(file_id, "Mandelbrotmenge", dataset, hdferr, H5P_DEFAULT_F)
        call H5Dget_space_f(dataset, filespace, hdferr)

        call H5Sget_simple_extent_dims_f(filespace, dimsr, maxdimsr, hdferr)
        info%width = int(dimsr(1),kind(info%width))
        info%height = int(dimsr(2),kind(info%height))

        ! read attributes
        dims(1) = 1
        call H5Aopen_f(dataset, "type", attr, hdferr, H5P_DEFAULT_F)
        call H5Aread_f(attr, H5T_NATIVE_INTEGER, info%type, dims, hdferr)
        call H5Aclose_f(attr, hdferr)
        call H5Aopen_f(dataset, "numprocs", attr, hdferr, H5P_DEFAULT_F)
        call H5Aread_f(attr, H5T_NATIVE_INTEGER, info%numprocs, dims, hdferr)
        call H5Aclose_f(attr, hdferr)
        call H5Aopen_f(dataset, "maxiter", attr, hdferr, H5P_DEFAULT_F)
        call H5Aread_f(attr, H5T_NATIVE_INTEGER, info%maxiter, dims, hdferr)
        call H5Aclose_f(attr, hdferr)
        dims(1) = 4
        call H5Aopen_f(dataset, "position", attr, hdferr, H5P_DEFAULT_F)
        call H5Aread_f(attr, H5T_NATIVE_REAL, attr_data, dims, hdferr)
        info%xmin = attr_data(1)
        info%xmax = attr_data(2)
        info%ymin = attr_data(3)
        info%ymax = attr_data(4)
        call H5Aclose_f(attr, hdferr)

        ! read data
        allocate(iterations(info%width*info%height))
        call H5Dread_f(dataset, H5T_NATIVE_INTEGER, iterations, dimsr, hdferr, H5S_ALL_F, H5S_ALL_F, H5P_DEFAULT_F)

        ! release resources
        call H5Sclose_f(filespace, hdferr)
        call H5Dclose_f(dataset, hdferr)
        call H5Fclose_f(file_id, hdferr)
        call H5close_f(hdferr)
    end subroutine collect_hdf5

end module mandelhdf5
