program mandelseq
    use infostruct
    use mandelsion
    use mandelmpiio
    use mandelhdf5
    use mandelpnetcdf
    use mandelnetcdf4
    use ppmwrite

    implicit none

    include "mpif.h"

    integer :: format = 0 ! type of output format: 0=sion, 1=hdf5, 2=mpiio, 3=pnetcdf

    integer :: nargs
    character(len=20) :: arg

    integer, dimension(:), pointer :: iterations
    integer, dimension(:), pointer :: proc_distribution

    integer :: i
    integer :: ierror

    type(t_infostruct) :: info

    call MPI_Init(ierror)

    nullify(proc_distribution)

    ! parse command line
    nargs = iargc()
    i = 1
    do while (i <= nargs)
        call getarg(i, arg)
        if (arg(1:1) == "-") then
            select case (arg(2:2))
                case ("f")
                    call getarg(i+1,arg)
                    read(unit=arg,fmt=*) format
                    i = i + 1
                case default
                    write(unit=0, fmt=*) arg
                    call getarg(0,arg)
                    call usage(arg);
                    stop 1
            end select
        else
            write(unit=0, fmt=*) arg
            call getarg(0,arg)
            call usage(arg);
            stop 1;
        end if
        i = i + 1
    end do

    ! init ppm
    call fppminitsmooth(1)

    ! print info header
    select case (format)
        case (0)
            print *, "using SIONlib"
        case (1)
            print *, "using HDF5"
        case (2)
            print *, "using MPIIO"
        case (3)
            print *, "using PNETCDF"
        case (4)
            print *, "using NETCDF4"
    end select

    ! read SION file
    select case (format)
        case (0)
            call collect_sion(iterations, proc_distribution, info)
        case (1)
            call collect_hdf5(iterations,info)
        case (2)
            call collect_mpiio(iterations,info)
        case (3)
            call collect_pnetcdf(iterations,info)
        case (4)
            call collect_netcdf4(iterations,info)
    end select

    ! print information
    call print_infostruct(info)

    if (associated(iterations)) then
        ! create ppm files
        call fppmwrite(iterations, info%width, info%height, 0, info%maxiter, "mandelcol.ppm")
        deallocate(iterations)
    end if

    if (associated(proc_distribution)) then
        call fppmwrite(proc_distribution, info%width, info%height, 0, info%numprocs, "mandelcol_procs.ppm")
        deallocate(proc_distribution)
    end if

    call MPI_Finalize(ierror)

contains
    subroutine usage(name)
        character(len=20), intent(in) :: name

        write(unit=0, fmt='(A,A,A,/)') "Usage: ", trim(name), " options"
        write(unit=0, fmt=*) "with the following optional options default values in parathesis):"
        write(unit=0, fmt=*) "  [-f <format>]             0=sion, 1=hdf5, 2=mpiio, 3=pnetcdf, 4=netcdf4"
    end subroutine usage

    ! Print infostruct data
    subroutine print_infostruct(info)
        type(t_infostruct), intent(in) :: info

        write(unit=*,fmt='(1x,a,i10)') "type      = ", info%type
        write(unit=*,fmt='(1x,a,i10)') "width     = ", info%width
        write(unit=*,fmt='(1x,a,i10)') "height    = ", info%height
        write(unit=*,fmt='(1x,a,i10)') "numprocs  = ", info%numprocs
        write(unit=*,fmt='(1x,a,f10.2)') "xmin      = ", info%xmin
        write(unit=*,fmt='(1x,a,f10.2)') "xmax      = ", info%xmax
        write(unit=*,fmt='(1x,a,f10.2)') "ymin      = ", info%ymin
        write(unit=*,fmt='(1x,a,f10.2)') "ymax      = ", info%ymax
        write(unit=*,fmt='(1x,a,i10)') "maxiter   = ", info%maxiter
    end subroutine print_infostruct
end program mandelseq
