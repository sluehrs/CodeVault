module infostruct
    implicit none

    include "mpif.h"

    private

    type, public :: t_infostruct
        sequence
        integer :: type
        integer :: width
        integer :: height
        integer :: numprocs
        real :: xmin
        real :: xmax
        real :: ymin
        real :: ymax
        integer :: maxiter
    end type t_infostruct

    type, public :: t_pos_struct
        sequence
        integer :: width
        integer :: height
        integer :: xpos
        integer :: ypos
    end type t_pos_struct

    public :: create_infostruct_datatype

contains
    subroutine create_infostruct_datatype(infostruct_mpi_datatype)
        integer, intent(out) :: infostruct_mpi_datatype
        integer :: tmp_datatype
        integer, dimension(9) :: blocklength = (/1, 1, 1, 1, 1, 1, 1, 1, 1/)
        integer(kind=MPI_ADDRESS_KIND), dimension(9) :: displs
        integer(kind=MPI_ADDRESS_KIND) :: base, extent
        integer, dimension(9) :: types = (/MPI_INTEGER, MPI_INTEGER, MPI_INTEGER, MPI_INTEGER, &
            MPI_REAL, MPI_REAL, MPI_REAL, MPI_REAL, MPI_INTEGER/)
        integer :: ierror
        type(t_infostruct), dimension(2) :: dummy_info

        call MPI_Get_address(dummy_info(1), base, ierror)
        call MPI_Get_address(dummy_info(1)%type, displs(1), ierror)
        call MPI_Get_address(dummy_info(1)%width, displs(2), ierror)
        call MPI_Get_address(dummy_info(1)%height, displs(3), ierror)
        call MPI_Get_address(dummy_info(1)%numprocs, displs(4), ierror)
        call MPI_Get_address(dummy_info(1)%xmin, displs(5), ierror)
        call MPI_Get_address(dummy_info(1)%xmax, displs(6), ierror)
        call MPI_Get_address(dummy_info(1)%ymin, displs(7), ierror)
        call MPI_Get_address(dummy_info(1)%ymax, displs(8), ierror)
        call MPI_Get_address(dummy_info(1)%maxiter, displs(9), ierror)
        displs = displs - base
        call MPI_Get_address(dummy_info(2), extent, ierror)
        extent = extent - base

        call MPI_Type_create_struct(9, blocklength, displs, types, tmp_datatype, ierror)

        call MPI_Type_create_resized(tmp_datatype, 0_MPI_ADDRESS_KIND, extent, infostruct_mpi_datatype, ierror)
        call MPI_Type_free(tmp_datatype, ierror)
        call MPI_Type_commit(infostruct_mpi_datatype, ierror)
    end subroutine create_infostruct_datatype
end module infostruct
