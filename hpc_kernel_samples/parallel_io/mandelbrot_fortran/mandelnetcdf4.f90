module mandelnetcdf4
    use infostruct
    use netcdf
    use mpi

    implicit none

    private

    public :: open_netcdf4, close_netcdf4, write_to_netcdf4_file, collect_netcdf4

contains

! open netcdf4 file
    subroutine open_netcdf4(fh, var_id, info, blocksize, start, rank)
        integer, intent(out) :: fh
        integer, intent(out) :: var_id
        type(t_infostruct), intent(in) :: info
        integer, dimension(2), intent(in) :: blocksize
        integer, dimension(2), intent(in) :: start
        integer, intent(in) :: rank

        integer :: err
        real, dimension(4) :: attr_real
        integer, dimension(2) :: dim_ids

        err = nf90_create_par("simple.nc",NF90_CLOBBER + NF90_NETCDF4 + NF90_MPIIO,MPI_COMM_WORLD,MPI_INFO_NULL,fh)

        err = nf90_def_dim(fh,"width",info%width,dim_ids(1))
        err = nf90_def_dim(fh,"height",info%height,dim_ids(2))

        err = nf90_put_att(fh,NF90_GLOBAL,"type",info%type)
        err = nf90_put_att(fh,NF90_GLOBAL,"numprocs",info%numprocs)
        err = nf90_put_att(fh,NF90_GLOBAL,"maxiter",info%maxiter)

        attr_real(1) = info%xmin
        attr_real(2) = info%xmax
        attr_real(3) = info%ymin
        attr_real(4) = info%ymax

        err = nf90_put_att(fh,NF90_GLOBAL,"position",attr_real)

        err = nf90_def_var(fh,"data",NF90_INT,dim_ids,var_id)

        err = nf90_enddef(fh)

        if (info%type == 0 .or. info%type == 1) then
            err = nf90_var_par_access(fh,var_id,NF90_COLLECTIVE)
        end if
    end subroutine open_netcdf4

    ! write block to netcdf4 file
    subroutine write_to_netcdf4_file(fh, var_id, info, iterations, width, height, xpos, ypos)
        integer, intent(in) :: fh
        integer, intent(in) :: var_id
        type(t_infostruct), intent(in) :: info
        integer, dimension(:), intent(inout) :: iterations
        integer, intent(in) :: width
        integer, intent(in) :: height
        integer, intent(in) :: xpos
        integer, intent(in) :: ypos

        integer, dimension(2) :: start
        integer, dimension(2) :: num
        integer err

        start(2) = ypos + 1
        start(1) = xpos + 1
        num(2) = height
        num(1) = width
        err = nf90_put_var(fh,var_id,iterations,start,num)
    end subroutine write_to_netcdf4_file

    ! close netcdf4 file
    subroutine close_netcdf4(fh, filetype, info, rank)
        integer, intent(inout) :: fh
        integer, intent(inout) :: filetype
        type(t_infostruct), intent(in) :: info
        integer, intent(in) :: rank

        integer :: err

        err = nf90_close(fh)
    end subroutine close_netcdf4

    ! read netcdf4 file
    subroutine collect_netcdf4(iterations, info)
        integer, dimension(:), pointer :: iterations
        type(t_infostruct), intent(inout) :: info

        integer :: fh
        integer :: var_id
        integer, dimension(2) :: dim_ids
        integer :: err
        integer :: offset
        integer, dimension(2) :: start
        integer, dimension(2) :: num
        real, dimension(4) :: attr_real

        err = nf90_open("simple.nc", NF90_NOWRITE, fh)

        err = nf90_inq_varid(fh,"data",var_id)
        err = nf90_inquire_variable(fh,var_id,dimids=dim_ids)
        err = nf90_inquire_dimension(fh,dim_ids(1),len=info%width)
        err = nf90_inquire_dimension(fh,dim_ids(2),len=info%height)

        err = nf90_get_att(fh,NF90_GLOBAL,"type",info%type)
        err = nf90_get_att(fh,NF90_GLOBAL,"numprocs",info%numprocs)
        err = nf90_get_att(fh,NF90_GLOBAL,"maxiter",info%maxiter)
        err = nf90_get_att(fh,NF90_GLOBAL,"position",attr_real)

        info%xmin = attr_real(1)
        info%xmax = attr_real(2)
        info%ymin = attr_real(3)
        info%ymax = attr_real(4)

        start(1) = 1
        start(2) = 1
        num(1) = info%width
        num(2) = info%height

        allocate(iterations(info%width*info%height))
        err = nf90_get_var(fh,var_id,iterations,start,num)

        err = nf90_close(fh)
    end subroutine collect_netcdf4

end module mandelnetcdf4

