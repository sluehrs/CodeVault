#ifndef INFOSTRUCT_H
#define INFOSTRUCT_H

#include <mpi.h>

typedef struct _infostruct
{
  int    type;
  int    width;
  int    height;
  int    numprocs;
  double xmin;
  double xmax;
  double ymin;
  double ymax;
  int    maxiter;
} _infostruct;

typedef struct _pos_struct
{
  int    width;
  int    height;
  int    xpos;
  int    ypos;
} _pos_struct;

void create_infostruct_datatype(MPI_Datatype *infostruct_mpi_datatype);

#endif
