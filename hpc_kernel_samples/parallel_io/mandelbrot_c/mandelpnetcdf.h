#ifndef MANDELPNETCDF_H
#define MANDELPNETCDF_H

#include "infostruct.h"

void write_to_pnetcdf_file(int *fh, int *var_id, const _infostruct *infostruct, const int *iterations, int width, int height,
                           int xpos, int ypos);

void open_pnetcdf(int *fh, int *var_id, const _infostruct *infostruct, const int *blocksize, const int *start, int rank);

void close_pnetcdf(int *fh, const _infostruct *infostruct, int rank);

void collect_pnetcdf(int **iterations, _infostruct *infostruct);

#endif
