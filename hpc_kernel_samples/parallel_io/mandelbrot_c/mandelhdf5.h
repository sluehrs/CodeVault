#ifndef MANDELHDF5_H
#define MANDELHDF5_H

#include <hdf5.h>
#include "infostruct.h"

void open_hdf5(hid_t *file_id, hid_t *dataset, const _infostruct *infostruct, const int *blocksize,
               const int *start, int rank);

void write_to_hdf5_file(hid_t *file_id, hid_t *dataset, const _infostruct *infostruct, const int *iterations,
                        int width, int height, int xpos, int ypos);

void close_hdf5(hid_t *file_id, hid_t *dataset, const _infostruct *infostruct, int rank);

void collect_hdf5(int **iterations, _infostruct *infostruct);

#endif
