#include <stdlib.h>
#include <mpi.h>

#include <hdf5.h>

#include "mandelhdf5.h"
#include "infostruct.h"

/* open HDF5 file */
void open_hdf5(hid_t *file_id, hid_t *dataset, const _infostruct *infostruct, const int *blocksize,
               const int *start, int rank)
{
  hid_t dcpl_id;
  hid_t plist_id;
  hid_t filespace;
  hid_t attr_dataspace_id;
  hid_t attribute_id;
  double attr_data[4];
  hsize_t dims;
  hsize_t chunk_dims[2];
  int data_rank;

  /* set up file access propterty */
  plist_id = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

  /* create new file collectively and release property list identifier */
  *file_id = H5Fcreate("simple.h5", H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
  H5Pclose(plist_id);

  if (infostruct->type == 2) {
    /* Create a dataset creation property list and set it to use chunking */
    dcpl_id = H5Pcreate(H5P_DATASET_CREATE);
    chunk_dims[0] = blocksize[0];
    chunk_dims[1] = blocksize[1];
    H5Pset_chunk(dcpl_id, 2, chunk_dims);
  } else {
    dcpl_id = H5P_DEFAULT;
  }

  /* create dataspace for dataset */
  hsize_t dimsf[] = {infostruct->height, infostruct->width};
  data_rank = sizeof(dimsf) / sizeof(hsize_t);
  filespace = H5Screate_simple(data_rank, dimsf, NULL);

  /* create dataset */
  *dataset = H5Dcreate(*file_id, "Mandelbrotmenge", H5T_NATIVE_INT, filespace, H5P_DEFAULT,
                       dcpl_id, H5P_DEFAULT);
  H5Sclose(filespace);
  if (infostruct->type == 2) {
    H5Pclose(dcpl_id);
  }

  /* Data space for attribute */
  dims = 1;
  attr_dataspace_id = H5Screate_simple(1, &dims, NULL);
  /* Write attributes */
  attribute_id = H5Acreate(*dataset, "type", H5T_STD_I32LE, attr_dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  H5Awrite(attribute_id, H5T_NATIVE_INT, &infostruct->type);
  H5Aclose(attribute_id);

  attribute_id = H5Acreate(*dataset, "numprocs", H5T_STD_I32LE, attr_dataspace_id, H5P_DEFAULT,
                           H5P_DEFAULT);
  H5Awrite(attribute_id, H5T_NATIVE_INT, &infostruct->numprocs);
  H5Aclose(attribute_id);

  attribute_id = H5Acreate(*dataset, "maxiter", H5T_STD_I32LE, attr_dataspace_id, H5P_DEFAULT,
                           H5P_DEFAULT);
  H5Awrite(attribute_id, H5T_NATIVE_INT, &infostruct->maxiter);
  H5Aclose(attribute_id);
  H5Sclose(attr_dataspace_id);

  dims = 4;
  attr_dataspace_id = H5Screate_simple(1, &dims, NULL);
  attribute_id = H5Acreate(*dataset, "position", H5T_IEEE_F64BE, attr_dataspace_id, H5P_DEFAULT,
                           H5P_DEFAULT);

  attr_data[0] = infostruct->xmin;
  attr_data[1] = infostruct->xmax;
  attr_data[2] = infostruct->ymin;
  attr_data[3] = infostruct->ymax;

  H5Awrite(attribute_id, H5T_NATIVE_DOUBLE, attr_data);
  H5Aclose(attribute_id);
  H5Sclose(attr_dataspace_id);
}

/* write block to HDF5 file */
void write_to_hdf5_file(hid_t *file_id, hid_t *dataset, const _infostruct *infostruct, const int *iterations,
                        int width, int height, int xpos, int ypos)
{
  int data_rank;
  hsize_t dimsm[1];
  hid_t plist_id;
  hid_t filespace, memspace;
  hsize_t offset[2], count[2], block[2], stride[2];

  /* create hyperslab */
  filespace = H5Dget_space(*dataset);

  offset[0] = ypos;
  offset[1] = xpos;
  count[0] = 1;
  count[1] = 1;
  block[0] = height;
  block[1] = width;
  stride[0] = height;
  stride[1] = width;

  H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, stride, count, block);

  data_rank = 1;
  dimsm[0] = width * height;
  memspace = H5Screate_simple(data_rank, dimsm, NULL);

  plist_id = H5Pcreate(H5P_DATASET_XFER);
  if (infostruct->type == 0 || infostruct->type == 1) {
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
  } else {
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_INDEPENDENT);
  }

  H5Dwrite(*dataset, H5T_NATIVE_INT, memspace, filespace, plist_id, iterations);

  /* release property list and filespace */
  H5Sclose(memspace);
  H5Sclose(filespace);
  H5Pclose(plist_id);
}

/* close HDF5 file */
void close_hdf5(hid_t *file_id, hid_t *dataset, const _infostruct *infostruct, int rank)
{
  H5Dclose(*dataset);
  H5Fclose(*file_id);
}

/* read HDF5 file */
void collect_hdf5(int **iterations, _infostruct *infostruct)
{
  hid_t file, dataset, filespace, attr;
  int attr_data_int;
  double attr_data_double[4];
  hsize_t dimsr[2];

  file = H5Fopen("simple.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
  dataset = H5Dopen(file, "Mandelbrotmenge", H5P_DEFAULT);
  filespace = H5Dget_space(dataset);

  H5Sget_simple_extent_dims(filespace, dimsr, NULL);
  infostruct->height = (int)dimsr[0];
  infostruct->width = (int)dimsr[1];

  /* read attributes */
  attr = H5Aopen(dataset, "type", H5P_DEFAULT);
  H5Aread(attr,H5T_NATIVE_INT,&attr_data_int);
  infostruct->type = attr_data_int;
  H5Aclose(attr);
  attr = H5Aopen(dataset, "numprocs", H5P_DEFAULT);
  H5Aread(attr,H5T_NATIVE_INT,&attr_data_int);
  infostruct->numprocs = attr_data_int;
  H5Aclose(attr);
  attr = H5Aopen(dataset, "maxiter", H5P_DEFAULT);
  H5Aread(attr,H5T_NATIVE_INT,&attr_data_int);
  infostruct->maxiter = attr_data_int;
  H5Aclose(attr);
  attr = H5Aopen(dataset, "position", H5P_DEFAULT);
  H5Aread(attr,H5T_NATIVE_DOUBLE,attr_data_double);
  infostruct->xmin = attr_data_double[0];
  infostruct->xmax = attr_data_double[1];
  infostruct->ymin = attr_data_double[2];
  infostruct->ymax = attr_data_double[3];
  H5Aclose(attr);

  /* read data */
  *iterations = malloc(sizeof(int) * infostruct->width * infostruct->height);
  H5Dread(dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, *iterations);

  /* release resources */
  H5Sclose(filespace);
  H5Dclose(dataset);
  H5Fclose(file);
}
