#ifndef MANDELNETCDF4_H
#define MANDELNETCDF4_H

#include "infostruct.h"

void write_to_netcdf4_file(int *fh, int *var_id, const _infostruct *infostruct, const int *iterations, int width, int height,
                           int xpos, int ypos);

void open_netcdf4(int *fh, int *var_id, const _infostruct *infostruct, const int *blocksize, const int *start, int rank);

void close_netcdf4(int *fh, const _infostruct *infostruct, int rank);

void collect_netcdf4(int **iterations, _infostruct *infostruct);

#endif
