#ifndef MANDELMPIIO_H
#define MANDELMPIIO_H

#include <mpi.h>
#include "infostruct.h"

void write_to_mpiio_file(MPI_File *fh, MPI_Datatype *filetype, const _infostruct *infostruct, int *iterations, int width, int height,
                         int xpos, int ypos);

void open_mpiio(MPI_File *fh, MPI_Datatype *filetype, _infostruct *infostruct, int *blocksize, int *start, int rank);

void close_mpiio(MPI_File *fh, MPI_Datatype *filetype, const _infostruct *infostruct, int rank);

void collect_mpiio(int **iterations, _infostruct *infostruct);

#endif
