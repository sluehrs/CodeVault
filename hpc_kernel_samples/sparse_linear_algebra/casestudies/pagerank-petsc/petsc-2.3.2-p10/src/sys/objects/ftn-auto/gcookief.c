#include "petsc.h"
#include "petscfix.h"
/* gcookie.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petsc.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define petscobjectexists_ PETSCOBJECTEXISTS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define petscobjectexists_ petscobjectexists
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   petscobjectexists_(PetscObject obj,PetscTruth *exists, int *__ierr ){
*__ierr = PetscObjectExists(
	(PetscObject)PetscToPointer((obj) ),exists);
}
#if defined(__cplusplus)
}
#endif
