#include "petsc.h"
#include "petscfix.h"
/* pinit.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petsc.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define petscinitialized_ PETSCINITIALIZED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define petscinitialized_ petscinitialized
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define petscfinalized_ PETSCFINALIZED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define petscfinalized_ petscfinalized
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   petscinitialized_(PetscTruth *isInitialized, int *__ierr ){
*__ierr = PetscInitialized(isInitialized);
}
void PETSC_STDCALL   petscfinalized_(PetscTruth *isFinalized, int *__ierr ){
*__ierr = PetscFinalized(isFinalized);
}
#if defined(__cplusplus)
}
#endif
