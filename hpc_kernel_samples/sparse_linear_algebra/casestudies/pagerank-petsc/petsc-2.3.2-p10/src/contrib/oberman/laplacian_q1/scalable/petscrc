###################################
######  Finite Element Code  ######
###################################

#### Mesh Size ####
  -nx 90
  -ny 90

#### Number of Subdomains ####
# nsdx must divide nx.
# nsdy must divide ny.
# nsdx*nsdy must equal the number of processors.
  -nsdx 3
  -nsdy 3

#### Boundary Conditions ####
# With the following flag, Dirichlet boundary condition is
# enforced only at the left edge of the rectangular domain
# (i.e., x==0). On the remaining of the border, homogeneous
# Neumann condition is enforced.
  -dirichlet_on_left
# Setting the Dirichlet b.c.
  -bc_pf_type              constant 
  -bc_pf_constant          1.0
  #-bc_pf_type             string
  #-bc_pf_string           f=cos(x*y)
# By default, the "unknowns" on the Dirichlet part of the border are
# assigned their correct value at the beginning of the code. The
# following flag enforces the use of a zero initial guess for these
# points also.
  #-use_zero_initial_guess

#### Load ####
  -rhs_pf_type             constant 
  -rhs_pf_constant         -2.0
  #-rhs_pf_type            string
  #-rhs_pf_string          f=cos(x*y)

#### Output ####
# The following flag creates the file solution.m with the solution
vector.
  -save_solution
# The following flag saves, in a file pc.m, the preconditioner matrix pc
# and the preconditioned operator BA. Note that both are not explicitly
# computed unless this option is activated. This option should only be
# used in rather small examples (and never when timing).
  #-save_global_preconditioner

########################################
######  Balancing Neumann-Neumann ######
########################################

#### Selecting the PC ####
# The use of mat_type is is mandatory for using the NN preconditioner.
  -mat_type               is
  -pc_type                nn

#### Damping Options #### 
# The default is damping (with 1.e-12) and removing nullspace of
# FLOATING subdomains ONLY. (It's a good default!)
  #-localN_pc_nn_damp_fixed                      1.e-12
  #-localN_pc_nn_remove_nullspace_fixed
  #-localN_pc_nn_set_damping_factor_floating     1.e-10
  #-localN_pc_nn_not_damp_floating
  #-localN_pc_nn_not_remove_nullspace_floating

#### Local Solvers ####
# The default is using exact local solvers (ksp_type==preonly and
pc_type==LU).
# Any PETSc's command-line options for SLES can be used here, with the
proper
# prefixes (localN_ for Neumann and localD_ for Dirichlet).
  #-localD_ksp_type                           gmres
  #-localD_ksp_max_it                            25
  #-localD_ksp_rtol                           1.e-3
  #-localD_pc_type                              ilu
  #-localD_pc_ilu_levels                          1
  #-localD_pc_ilu_mat_ordering_type              nd
  #-localN_ksp_type                           gmres
  #-localN_ksp_max_it                            25
  #-localN_ksp_rtol                           1.e-3
  #-localN_pc_type                              ilu
  #-localN_pc_ilu_levels                          1
  #-localN_pc_ilu_mat_ordering_type              nd

#### Balancing ####
  #-turn_off_first_balancing
  #-turn_off_second_balancing

#### Output ####
# The following flag creates the file coarse.m with the coarse matrix.
  #-save_coarse_matrix
# This option causes the program to check for consistency of the Neumann
# problems. It prints the average of the rhs vector (since consistency,
# for the floating subdomains, would mean zero average).
  #-check_consistency

###############################
######  General Options  ######
###############################

#### Profiling ####
# When this flag is true, the core of the program is run twice, in order
# to avoid the paging time to interfere in the performance.
  -preload                 FALSE
  #-log_summary
  #-log_sync
  #-get_resident_set_size

#### KSP ####
  -ksp_type               gmres
  -ksp_truemonitor
  -ksp_compute_eigenvalues
  -ksp_rtol               1.e-12


