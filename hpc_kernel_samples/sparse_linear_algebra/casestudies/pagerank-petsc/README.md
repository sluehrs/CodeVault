=======
README
=======
- 1. Code sample name
pagerank

- 2. Description of the code sample package
pagerank code calculates ranking of pages. The link information of the pages is given as a graph. The original code is taken from [1].

This package contains the following important directories:
1) pagerank: Implementation of the PageRank algorithm. This code is developed by D. Gleich, L. Zhukov, and P. Berkhin. No license information is given on the download page [1]. C programming language is used.
2) petsc-2.3.2-p10: PETSc version 2.3.2-p10, which is used to implement the PageRank algorithm.
3) pagerank/boost: The Boost library. Some of its utilities are used in the pagerank code.
4) pagerank/data: The input files are stored in this directory.

The implementation of the kernel algorithm is located at line 278 of ppagerank.cc file. This file is found under the directory pagerank.

This is one of the code sample from the PRACE CodeVault. You can find more code samples available for download from the PRACE CodeVault here: https://gitlab.com/PRACE-4IP/CodeVault 

- 3. Release date
08 April 2016

- 4. Version history 
1.0: initial version

- 5. Contributor (s) / Maintainer(s) 
Kadir Akbudak, Bilkent University <kadir.cs@gmail.com>
Cevdet Aykanat, Bilkent University <aykanat@cs.bilkent.edu.tr>

- 6. Copyright / License of the code sample
Apache 2.0

- 7. Language(s) 
C

- 8. Parallelisation Implementation(s)
MPI

- 9. Level of the code sample complexity 
advanced

- 10. Instructions on how to compile the code
Pre-requisities are gcc, make, Bash, and an MPI library.

Compilation of the package contains two steps:
1) For compilation of PETSc, run the following commands in Bash:
cd petsc-2.3.2-p10
./config/configure
PETSC_DIR= <workingdir>/petsc-2.3.2-p10
export PETSC_DIR
PETSC_ARCH=linux-gnu-c-debug
export PETSC_ARCH
make
cd ..

2) For compilation of pagerank, run the following commands in Bash:
cd pagerank
Set the variable PETSC_DIR in the makefile as ../petsc-2.3.2-p10.
make

- 11. Instructions on how to run the code
Run the script named run.sh. The script contains the following line:
    mpirun -np 4 ./ppagerank -m data/example-tiny.graph
The only parameter is the input data.

- 12. Sample input(s)
Data files are located under the folder named data. There is only one test data in this folder. More files can be obtained from [2].

- 13. Sample output(s)
The program prints the PageRank vector. This vector contains the rank of each page.

REFERENCES:
[1] https://github.com/dgleich/ppagerank
[2] https://www.cs.purdue.edu/homes/dgleich/codes/innout/innout-web.tar.gz

