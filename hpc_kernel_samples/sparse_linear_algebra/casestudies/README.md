CodeVault: Case Studies for Sparse Linear Algebra
================
# Overview
In this case studies folder, there are sample codes for beginners, as well as advanced codes including important algorithm, which include sparse linear algebra

# Contributors & Maintainers
- Cevdet Aykanat (aykanat@cs.bilkent.edu.tr) 
- Kadir Akbudak (kadir.cs@gmail.com) 
- Reha Oguz Selvitopi(reha@cs.bilkent.edu.tr) 

# Contents
- pagerank:  calculates ranking of pages. Sparse linear algebra is implemented via PETSc.
