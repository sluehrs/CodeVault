CodeVault: Sparse Matrix-Vector Multiplication
================
# Overview
Sparse matrix-vector multiplication (SpMV) is one of the fundamental kernels in scientific computing. This folder contains samples for performing SpMV.

# Contributors & Maintainers
- Cevdet Aykanat (aykanat@cs.bilkent.edu.tr) 
- Kadir Akbudak (kadir.cs@gmail.com) 
- Reha Oguz Selvitopi(reha@cs.bilkent.edu.tr) 

