#!/usr/bin/env python 
import multiprocessing
import subprocess
import sys
import os
import datetime
import shutil
import json, ast
from pprint import pprint
from itertools import repeat,izip


MP = False
MP = True

def read_json(filename):
  f = open(filename,'r')
  data = json.load(f)
  f.close()
  return data
  
def run_command(cmd):
  try:
    process = subprocess.Popen(cmd, stdin=None,
                      stdout=subprocess.PIPE,
                      stderr=subprocess.PIPE)
  except:
    print ("ERROR: The child (%s) raised an exception: %s\n" % (cmd, sys.exc_info()[1]))
    raise

  while False:
    nextline = process.stdout.readline()
    if nextline == '' and process.poll() != None:
      break
    sys.stdout.write(nextline)
    sys.stdout.flush()

  out = process.communicate()
  stdout = out[0].decode("utf-8")
  stderr = out[1].decode("utf-8")
  retval = process.returncode
  return (retval, stdout, stderr)

def build_one_dwarf_pool(input_args): 
  compiler_folder, compiler, dwarf, dwarflenmax, num_make = input_args
  build_one_dwarf(compiler_folder, compiler, dwarf, dwarflenmax, num_make)

def build_one_dwarf(compiler_folder, compiler, dwarf, dwarflenmax, num_make):
  dwarf_folder = compiler_folder + "/"+ dwarf.replace("/","_")
  os.makedirs(dwarf_folder+"/build")
  os.makedirs(dwarf_folder+"/install")
  script_name="build_script.sh"
  f = open(dwarf_folder+"/"+script_name, 'w')
  f.write('#!/bin/bash \n')
  command = 'cd ' + dwarf_folder + '/build && '
  command += 'CC='+CC+ ' ' if CC != None else ''
  command += 'CXX='+CXX+ ' ' if CXX != None else ''
  command += 'FC='+FC+ ' ' if FC != None else ''
  command += 'GNU_CXX='+GNU_CXX+ ' ' if GNU_CXX != None else ''
  command += 'cmake '+root_dir+("/../"+dwarf if dwarf != 'codevault' else '/../')+ ' '
  command += '-DCMAKE_INSTALL_PREFIX=$PWD/../install '
  command += '&& make -j '+str(num_make) + ' && make install'
  f.write(command+'\n');
  f.close();

  dwarf_ext = ''
  for i in range(len(dwarf),dwarflenmax+2):
    dwarf_ext += ' '
  dwarf_ext += '"'+dwarf + '"'

  printstr = " * Built " + dwarf_ext + " with " + compiler + ": "
  retval, out, err = run_command(['bash',dwarf_folder+"/"+script_name])
  logfile = dwarf_folder+"/"+"build.log"
  fout = open(logfile,"w")
  fout.write("=======BUILD COMMAND=====\n")
  fout.write(command+"\n");
  fout.write("=======STDERR=======\n")
  fout.write(err.encode('utf-8'));
  fout.write("=======STDOUT=======\n")
  fout.write(out.encode('utf-8'));
  fout.close();
  if retval != 0:
    printstr += " *FAIL* "
  else:
    printstr += "   OK   "
  printstr += " ... logfile:  " + dwarf_folder + "/build.log"
  sys.stdout.write(printstr+"\n");
  sys.stdout.flush()


def build_dwarfs(argv,num_jobs,num_make):
  compiler_list = {
      'GNU': {'cxx':'g++', 'cc':'gcc', 'fortran':'gfortran'}
    }

  dwarf_list = [
      # build all CodeVault
      'codevault',

      # build each of the Dwarfs
      '1_dense',
      '3_spectral',
      '4_nbody',
      '8_IO',

      # build contributions inside each of the dwarfs, were applicable
      # 4_nbody
      '4_nbody/hermite4',
      '4_nbody/bhtree'
      ]


  dwarf_list = read_json(argv[1])
  if len(argv) >= 3:
    compiler_list = read_json(argv[2]);
  else:
    sys.stdout.write("No compilers.json proviled, will be using default compilers.\n");

  sys.stdout.write("Going to build the following dwarfs: \n")
  for dwarf in dwarf_list:
    sys.stdout.write("  %s \n" % (dwarf if len(dwarf)>0 else '*everything*'));
  sys.stdout.write("-------------------------\n");

  sys.stdout.write("Using the following compilers: \n")
  pprint(ast.literal_eval(json.dumps(compiler_list)))

  sys.stdout.write("-------------------------\n");


  # set GNU_CXX needed for 4_nbody/bhtree
  global GNU_CXX
  GNU_CXX = compiler_list['GNU']['cxx'] if 'GNU' in compiler_list else None

  dwarflenmax = 0;
  for s in dwarf_list:
    dwarflenmax = max(dwarflenmax,len(s))

  build_folder ="_build_" + datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
  os.makedirs(build_folder)
     
  # Get location of this python program [this assume it is in the top source tree of CodeVault folder] 
  global root_dir
  root_dir = os.path.dirname(os.path.abspath(__file__))
  print "Build_folder: " + build_folder
  print "----------------------------------"
  f = open(build_folder+"/dwarfs.json","w")
  f.write(json.dumps(dwarf_list,indent=4));
  f.close()
  f = open(build_folder+"/compilers.json","w")
  f.write(json.dumps(compiler_list,indent=4));
  f.close()

  for compiler in compiler_list:
    global CXX, CC, FC
    CXX=compiler_list[compiler]['cxx'    ] if 'cxx'     in compiler_list[compiler] else None
    CC =compiler_list[compiler]['cc'     ] if 'cc'      in compiler_list[compiler] else None
    FC =compiler_list[compiler]['fortran'] if 'fortran' in compiler_list[compiler] else None
    compiler_folder = build_folder + "/" + compiler;
    os.makedirs(compiler_folder);
    if MP:
      pool = multiprocessing.Pool(num_jobs)
      pool.map(build_one_dwarf_pool, izip(repeat(compiler_folder), repeat(compiler), dwarf_list, repeat(dwarflenmax), repeat(num_make)))
    else:
      for dwarf in dwarf_list:
        build_one_dwarf(compiler_folder, compiler, dwarf, dwarflenmax, repeat(num_make))

    print "------------------------------------------"


def rebuild_one_dwarf_pool(input_args): 
    folder, dwarf, dwarflenmax, logfile, num_make = input_args
    rebuild_one_dwarf(folder,dwarf,dwarflenmax, logfile, num_make)

def rebuild_one_dwarf(folder, dwarf, dwarflenmax, logfile, num_make):
    script_name="build_script.sh"
    dwarf_folder = build_folder + "/"+folder+"/"+dwarf+"/";
    f = open(dwarf_folder+script_name, 'w')
    f.write('#!/bin/bash \n')
    command = 'cd ' + dwarf_folder + '/build && make -j '+str(num_make)+' && make install '
    f.write(command+'\n');
    f.close();

    dwarf_ext = ''
    for i in range(len(dwarf),dwarflenmax):
      dwarf_ext += ' '
    dwarf_ext += '"'+dwarf + '"'
    printstr = " * Re-made " + dwarf_ext + " with " + folder + ": "
    sys.stdout.flush()
    retval, out, err = run_command(['bash',dwarf_folder+"/"+script_name])
    fout = open(dwarf_folder+logfile,"w")
    fout.write("=======BUILD COMMAND=====\n")
    fout.write(command+"\n");
    fout.write("=======STDERR=======\n")
    fout.write(err.encode('utf-8'));
    fout.write("=======STDOUT=======\n")
    fout.write(out.encode('utf-8'));
    fout.close();
    if retval != 0:
      printstr += " *FAIL* "
    else:
      printstr += "   OK   "
    printstr += " ... logfile:  " + dwarf_folder + logfile
    sys.stdout.write(printstr+"\n");
    sys.stdout.flush()

def rebuild_dwarfs(build_folder, num_jobs, num_make):
  print 'Build folder "'+build_folder+'" is found. Attempting to re-make everything...'
  print "------------------------------------------"
  folder_list= next(os.walk(build_folder))[1]
  for folder in folder_list:
    logfile ='log_'+datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

    dwarf_list = next(os.walk(build_folder+"/"+folder))[1]
    dwarflenmax = 0;
    for s in dwarf_list:
      dwarflenmax = max(dwarflenmax,len(s))
    if MP:
      pool = multiprocessing.Pool(num_jobs)
      pool.map(rebuild_one_dwarf_pool, izip(repeat(folder), dwarf_list, repeat(dwarflenmax), repeat(logfile), repeat(num_make)))
    else:
      for dwarf in dwarf_list:
        rebuild_one_dwarf(folder, dwarf, dwarflenmax, logfile, num_make)
    print "------------------------------------------"


  

  sys.exit(0)

if __name__ == "__main__":

  if len(sys.argv) <  2:
    sys.stdout.write("Usage: \n\n");
    sys.stdout.write("  * To build CodeVault in a new build folder with cmake && make && make install use: \n")
    sys.stdout.write("      \"%s  dwarfs_to_build.json [compilers.json] [-j=num_jobs_total or -j] [-k=num_jobs_make]\" \n" % sys.argv[0])
    sys.stdout.write("\n")
    sys.stdout.write("  * To re-make existing build folder from the step above: \n")
    sys.stdout.write("      \"%s  build_folder [-j=num_jobs_total or -j] [-k=num_jobs_make]\" \n" % sys.argv[0])
    sys.stdout.write("\n")
    sys.exit(0)

  argv = []
  num_jobs = -1
  num_make = -1
  for arg in sys.argv:
    if arg[0:2]=='-j':
      if len(arg) == 2:
        num_jobs = multiprocessing.cpu_count()
        if num_make == -1:
          num_make = 4
      elif arg[2] == '=':
        num_jobs = int(arg[3:])
    elif arg[0:3]=='-k=':
        num_make = int(arg[3:])
        if num_jobs == -1:
          num_jobs = multiprocessing.cpu_count()
    else:
      argv += [arg]

  num_make = min(max(num_make,1),multiprocessing.cpu_count())
  num_jobs = max(num_jobs / num_make,1)

  print 'Using ' + str(num_jobs) + ' parallel jobs'
  print 'Using ' + str(num_make) + ' parallel makes'

  build_folder = argv[1];
  if os.path.isdir(build_folder) and os.path.exists(build_folder):
    rebuild_dwarfs(build_folder, num_jobs, num_make)
  else:
    build_dwarfs(argv, num_jobs, num_make)


