
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Evghenii Gaburov <evghenii.gaburov@surfsara.nl>
#
# ==================================================================================================

# CMake project
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 4_nbody)
endif()

set(NAME ${DWARF_PREFIX}_naive)


# Add the examples
# TODO: add chapel somehow
add_subdirectory(boost_mpi_open_mp)
add_subdirectory(open_mp)
add_subdirectory(shared_memory_window)
# ==================================================================================================
