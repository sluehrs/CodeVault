# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Paul Heinzlreiter <paul.heinzlreiter@risc-software.at>
#
# ==================================================================================================

cmake_minimum_required (VERSION 3.0 FATAL_ERROR)
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
set(CXX11 ${COMPILER_SUPPORTS_CXX11})
set(CXX11_FLAGS -std=c++11)
set(CMAKE_CXX_FLAGS "-std=c++11") 
file( GLOB datastructures_SOURCES *.hpp *.cpp )
add_library( datastructures ${datastructures_SOURCES} )


