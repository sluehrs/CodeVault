
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Evghenii Gaburov <evghenii.gaburov@surfsara.nl>
#
# ==================================================================================================

# CMake project
cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)
project("Hermite4" NONE)
include(${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 4_nbody)
endif()
set(NAME ${DWARF_PREFIX}_hermite4)

enable_language(CXX)

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
set(CXX11 ${COMPILER_SUPPORTS_CXX11})
set(CXX11_FLAGS -std=c++11)

if (NOT CXX11)
  message("## Skipping '${NAME}': no C++11 support")
  install(CODE "MESSAGE(\"${NAME} can only be built with C++11.\")")
  return()
endif()

message("** Enabling '${NAME}' serial example")

find_package(Common) 
select_compiler_flags(cxx_flags 
    GNU   "-Wall -Werror ${CXX11_FLAGS}"
    CLANG "-Wall -Werror ${CXX11_FLAGS}"
    Intel "-Wall -Werror ${CXX11_FLAGS}"
    PGI   "${CXX11_FLAGS}")

add_executable(${NAME}_serial hermite4_serial.cpp)
set_target_properties(${NAME}_serial PROPERTIES COMPILE_FLAGS ${cxx_flags})
install(TARGETS ${NAME}_serial DESTINATION bin)


find_package(OpenMP)

if (NOT ${OPENMP_FOUND})
  message("## Skipping '${NAME}_omp': no OpenMP support found")
  return()
endif()

message("** Enabling '${NAME}' OpenMP example")

# Build OpenMP example
add_executable(${NAME}_omp hermite4_omp.cpp)
set_target_properties(${NAME}_omp PROPERTIES COMPILE_FLAGS "${cxx_flags} ${OpenMP_CXX_FLAGS}")
set_target_properties(${NAME}_omp PROPERTIES LINK_FLAGS "${OpenMP_CXX_FLAGS}")
install(TARGETS ${NAME}_omp DESTINATION bin)

find_package(ISPC)
if (NOT ${ISPC_FOUND})
  message("## Skipping '${NAME}_ispc': no ISPC compiler found")
  return()
endif()

message("** Enabling '${NAME}' ISPC example")
# Build ISPC example
set (ispc_obj "")
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  ispc_compile(ispc_obj ispc/hermite4.ispc COMPILE_FLAGS "-O2 --opt=fast-math --math-lib=svml")
else()
  ispc_compile(ispc_obj ispc/hermite4.ispc COMPILE_FLAGS "-O2 --opt=fast-math")
endif()

add_executable(${NAME}_ispc
  ${ispc_obj} 
  ispc/typeReal.h
  ispc/hermite4.ispc
  ispc/hermite4.cpp 
  ispc/omp_tasksys.cpp)
set_property(TARGET ${NAME}_ispc  APPEND_STRING PROPERTY COMPILE_FLAGS " ${OpenMP_CXX_FLAGS}")
set_property(TARGET ${NAME}_ispc  APPEND_STRING PROPERTY COMPILE_FLAGS " ${cxx_flags}")
set_target_properties(${NAME}_ispc PROPERTIES LINK_FLAGS "${OpenMP_CXX_FLAGS}")
install(TARGETS ${NAME}_ispc DESTINATION bin)


# ==================================================================================================
